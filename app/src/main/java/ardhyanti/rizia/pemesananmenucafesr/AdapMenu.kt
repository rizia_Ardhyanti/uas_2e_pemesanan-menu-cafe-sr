package ardhyanti.rizia.pemesananmenucafesr

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.tambah_menu.*

class AdapMenu(val dataMenu: List<HashMap<String,String>>, val menu: Menu): RecyclerView.Adapter<AdapMenu.HolderDataMenu> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapMenu.HolderDataMenu {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.ls_menu,parent,false)
        return HolderDataMenu(v)
    }

    override fun getItemCount(): Int {
        return dataMenu.size
    }

    override fun onBindViewHolder(holder: AdapMenu.HolderDataMenu, position: Int) {
        val data = dataMenu.get(position)
        holder.txNama.setText(data.get("nama_menu"))
        holder.txKategori.setText(data.get("nama_kategori"))
        holder.txHarga.setText(data.get("harga"))
        if(!data.get("url").equals(""))
            Picasso.get().load(data.get("url")).into(holder.photo)

        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            val pos = menu.daftarKategori.indexOf(data.get("nama_kategori"))
            menu.spinner.setSelection(pos)
            menu.edNama.setText(data.get("nama_menu"))
            menu.edHarga.setText(data.get("harga"))
            Picasso.get().load(data.get("url")).into(menu.imgUpload)

        })
    }

    class HolderDataMenu(v : View) :  RecyclerView.ViewHolder(v){
        val txNama = v.findViewById<TextView>(R.id.txNama)
        val txKategori = v.findViewById<TextView>(R.id.txKategori)
        val txHarga = v.findViewById<TextView>(R.id.txHarga)
        val photo = v.findViewById<ImageView>(R.id.imageView)
        val layouts = v.findViewById<ConstraintLayout>(R.id.rowmenu)
    }

}