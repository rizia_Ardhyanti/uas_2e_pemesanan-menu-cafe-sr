package ardhyanti.rizia.pemesananmenucafesr

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.kategori.*

class AdapKategori(val datakat : List<HashMap<String,String>>, val kategori : Kategori) : RecyclerView.Adapter<AdapKategori.HolderKategori> () {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): AdapKategori.HolderKategori {
        val v = LayoutInflater.from(parent.context).inflate(R.layout.ls_kategori,parent,false)
        return HolderKategori(v)
    }

    override fun getItemCount(): Int {
        return datakat.size
        }

    override fun onBindViewHolder(holder: AdapKategori.HolderKategori, position: Int) {
        val data = datakat.get(position)
        holder.txkat.setText(data.get("nama_kategori"))
        holder.txid.setText(data.get("id_kategori"))
        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            kategori.namaKat.setText(data.get("nama_kategori"))
            kategori.id_kategori = data.get("id_kategori").toString()

        })

        }

    class HolderKategori(v : View) :  RecyclerView.ViewHolder(v){
        val txid = v.findViewById<TextView>(R.id.idKtg)
        val txkat = v.findViewById<TextView>(R.id.Ktg)
        val layouts = v.findViewById<ConstraintLayout>(R.id.row_kat)
    }

}