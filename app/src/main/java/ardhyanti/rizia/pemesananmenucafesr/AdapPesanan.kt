package ardhyanti.rizia.pemesananmenucafesr

import android.graphics.Color
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.recyclerview.widget.RecyclerView
import kotlinx.android.synthetic.main.pemesanan.*
import kotlinx.android.synthetic.main.tambah_menu.edHarga

class AdapPesanan(val dataPesanan: List<HashMap<String,String>>, val pesan: Pesanan): RecyclerView.Adapter<AdapPesanan.HolderDataPesanan> (){
    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): AdapPesanan.HolderDataPesanan {
        val v  = LayoutInflater.from(parent.context).inflate(R.layout.ls_pemesanan,parent,false)
        return HolderDataPesanan(v)
    }

    override fun getItemCount(): Int {
        return dataPesanan.size
    }

    override fun onBindViewHolder(holder: AdapPesanan.HolderDataPesanan, position: Int) {
        val data = dataPesanan.get(position)
        holder.txNoMeja.setText(data.get("nomer_meja"))
        holder.txMenu.setText(data.get("nama_menu"))
        holder.txHarga.setText(data.get("harga"))
        holder.txTotal.setText(data.get("total"))


        if (position.rem(2) == 0) holder.layouts.setBackgroundColor(Color.rgb(230,245,240))
        else holder.layouts.setBackgroundColor(Color.rgb(255,255,245))

        holder.layouts.setOnClickListener(View.OnClickListener{
            val pos = pesan.daftarMenu.indexOf(data.get("nama_menu"))
            pesan.spinner2.setSelection(pos)
            pesan.edNoMeja.setText(data.get("nomer_meja"))
            pesan.edHarga.setText(data.get("harga"))
            pesan.edTotal.setText(data.get("total"))


        })
    }

    class HolderDataPesanan(v : View) :  RecyclerView.ViewHolder(v){
        val txNoMeja = v.findViewById<TextView>(R.id.txMeja)
        val txMenu = v.findViewById<TextView>(R.id.txPesan)
        val txHarga = v.findViewById<TextView>(R.id.txHarga2)
        val txTotal = v.findViewById<TextView>(R.id.txTotal)
        val layouts = v.findViewById<ConstraintLayout>(R.id.rowPesan)
    }

}