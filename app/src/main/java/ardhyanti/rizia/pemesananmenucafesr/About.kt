package ardhyanti.rizia.pemesananmenucafesr

import android.content.Intent
import android.content.SharedPreferences
import android.graphics.Color
import android.os.Bundle
import android.view.Menu
import android.view.MenuItem
import androidx.appcompat.app.AppCompatActivity
import android.content.Context
import kotlinx.android.synthetic.main.profil.*

class About: AppCompatActivity(){

    lateinit var  Preference : SharedPreferences
    var PrefKey  = "setting"
    val font_size = "font_size_header"
    val default_font_header = 24
    val  teks  = "teks_header"
    val def_head = "CAFE SR"
    val teks_isi = "teks_isi"

    var  defcolhet = ""  //warna header
    var  defcolbacg= ""  //warna background

    override fun onCreateOptionsMenu(menu: Menu?): Boolean {
        var mnuInflater = menuInflater
        mnuInflater.inflate(R.menu.setting,menu)
        return super.onCreateOptionsMenu(menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        when(item?.itemId){
            R.id.itemsetting ->{
                var intent = Intent(this, SettingActivity::class.java)
                //startActivityForResult(intent,RC_PENGATURAN)
                startActivity(intent)
                return true
            }
        }
        return super.onOptionsItemSelected(item)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.profil)

    Preference = getSharedPreferences(PrefKey, Context.MODE_PRIVATE)
    header.setText(Preference.getString(teks,def_head))
    header.textSize = Preference.getInt(font_size,default_font_header).toFloat()
    defcolhet = Preference.getString("warnahed","kuning").toString()
    defcolbacg= Preference.getString("warnabacg","biru").toString()
    //header.setBackgroundColor=Preference.getString(col_head,"")
    isi.setText(Preference.getString(teks_isi,"Tempat Nongkrong kekinian Free WiFi, Nyaman, Murah, Smoking Area"))

    WarnaHeader()
    warnaBacgroun()
}

fun WarnaHeader(){
    if(defcolhet=="biru"){
        header.setBackgroundColor(Color.BLUE)
    }
    else if(defcolhet=="kuning"){
        header.setBackgroundColor(Color.YELLOW)
    }
    else if(defcolhet=="hijau"){
        header.setBackgroundColor(Color.GREEN)
    }
    else if(defcolhet=="hitam"){
        header.setBackgroundColor(Color.BLACK)
    }
}

fun warnaBacgroun(){
    if(defcolbacg=="Blue"){
        layoutq.setBackgroundColor(Color.BLUE)
    }
    else if(defcolbacg=="Yellow"){
        layoutq.setBackgroundColor(Color.YELLOW)
    }
    else if(defcolbacg=="Green"){
        layoutq.setBackgroundColor(Color.GREEN)
    }
    else if(defcolbacg=="Black"){
        layoutq.setBackgroundColor(Color.BLACK)
    }
}

}
