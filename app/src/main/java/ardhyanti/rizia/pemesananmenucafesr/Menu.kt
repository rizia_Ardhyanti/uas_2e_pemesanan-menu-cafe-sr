package ardhyanti.rizia.pemesananmenucafesr

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.kategori.*
import kotlinx.android.synthetic.main.tambah_menu.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class Menu : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.imgUpload -> {
                val intent = Intent()
                intent.setType("image/*")
                intent.setAction(Intent.ACTION_GET_CONTENT)
                startActivityForResult(intent,mediaHelper.getRcGallery())
            }
            R.id.btnSave ->{
                query("insert")
            }
        }
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var menuAdapter : AdapMenu
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarMenu = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    val url = "http://192.168.43.239/UasAndroid/show_data.php"
    val url2 = "http://192.168.43.239/UasAndroid/show_kategori.php"
    val url3 = "http://192.168.43.239/UasAndroid/query_data.php"
    var imStr = ""
    var pilihKategori=""
    var id_menu : String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.tambah_menu)
        menuAdapter = AdapMenu(daftarMenu, this)
        mediaHelper = MediaHelper(this)
        lsMenu.layoutManager = LinearLayoutManager(this)
        lsMenu.adapter = menuAdapter

        kategoriAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarKategori)
        spinner.adapter = kategoriAdapter
        spinner.onItemSelectedListener = itemSelected

        imgUpload.setOnClickListener(this)
        btnSave.setOnClickListener(this)

    }

    override fun onStart() {
        super.onStart()
        showDataMenu("")
        getNamaKategori()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihKategori = daftarKategori.get(position)
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr =  mediaHelper.getBitmapToString(data!!.data, imgUpload)
            }
        }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this, "Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataMenu("")
                }else{
                    Toast.makeText(this, "Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG)
                    .show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                val nmFile = "DC"+ SimpleDateFormat("yyyyMMddHHmmss", Locale.getDefault())
                    .format(Date())+".jpg"
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nama_menu", edNama.text.toString())
                        hm.put("harga", edHarga.text.toString())
                        hm.put("image", imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_kategori", pilihKategori)
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_menu", id_menu)
                        hm.put("nama_menu", edNama.text.toString())
                        hm.put("harga", edHarga.text.toString())
                        hm.put("image", imStr)
                        hm.put("file",nmFile)
                        hm.put("nama_kategori", pilihKategori)
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("nama_menu", id_menu)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getNamaKategori(){
        val request = StringRequest(Request.Method.POST,url2,
            Response.Listener { response ->
                daftarKategori.clear()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarKategori.add(jsonObject.getString("nama_kategori"))
                }
                kategoriAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataMenu(namaMenu : String){
        val request = object : StringRequest(Request.Method.POST,url,
            Response.Listener { response ->
                daftarMenu.clear()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mn = HashMap<String,String>()
                    mn.put("id_menu",jsonObject.getString("id_menu").toString())
                    mn.put("nama_menu",jsonObject.getString("nama_menu"))
                    mn.put("harga",jsonObject.getString("harga"))
                    mn.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    mn.put("url",jsonObject.getString("url"))
                    daftarMenu.add(mn)
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server",Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama_menu", namaMenu)
                return  hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


}