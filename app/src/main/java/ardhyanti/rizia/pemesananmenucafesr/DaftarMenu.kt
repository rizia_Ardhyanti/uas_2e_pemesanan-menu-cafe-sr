package ardhyanti.rizia.pemesananmenucafesr

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.MotionEvent
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.tambah_menu.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class DaftarMenu: AppCompatActivity(), View.OnClickListener {

    override fun onClick(v: View?) {
        TODO("Not yet implemented")
    }

    lateinit var mediaHelper: MediaHelper
    lateinit var menuAdapter : AdapMenu
    lateinit var kategoriAdapter : ArrayAdapter<String>
    var daftarMenu = mutableListOf<HashMap<String,String>>()
    var daftarKategori = mutableListOf<String>()
    val url = "http://192.168.43.239/UasAndroid/show_data.php"
    val url2 = "http://192.168.43.239/UasAndroid/show_kategori.php"
    val url3 = "http://192.168.43.239/UasAndroid/query_data.php"
    var imStr = ""
    var pilihKategori=""
    var id_menu : String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.daftar_menu)

        mediaHelper = MediaHelper(this)
        lsMenu.layoutManager = LinearLayoutManager(this)
        lsMenu.adapter = menuAdapter
        lsMenu.addOnItemTouchListener(itemTouch)
    }
    override fun onStart() {
        super.onStart()
        showDataMenu("")
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner.setSelection(0)
            pilihKategori = daftarKategori.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {

        }
    }
    val itemTouch = object : RecyclerView.OnItemTouchListener{
        override fun onTouchEvent(rv: RecyclerView, e: MotionEvent) {
        }

        override fun onInterceptTouchEvent(rv: RecyclerView, e: MotionEvent): Boolean {
            val view = rv.findChildViewUnder(e.x,e.y)
            val tag = rv.getChildAdapterPosition(view!!)
            val pos = daftarKategori.indexOf(daftarMenu.get(tag).get("nama_kategori"))

            spinner.setSelection(pos)
            edNama.setText(daftarMenu.get(tag).get("nama_menu").toString())
            edHarga.setText(daftarMenu.get(tag).get("harga").toString())
            Picasso.get().load(daftarMenu.get(tag).get("url")).into(imgUpload)
            return false
        }

        override fun onRequestDisallowInterceptTouchEvent(disallowIntercept: Boolean) {
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if(resultCode == Activity.RESULT_OK){
            if(requestCode == mediaHelper.getRcGallery()){
                imStr =  mediaHelper.getBitmapToString(data!!.data, imgUpload)
            }
        }
    }

    fun showDataMenu(namaMenu : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarMenu.clear()
                //Toast.makeText(this,"koneksi ke server",Toast.LENGTH_SHORT).show()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mn = HashMap<String,String>()
                    mn.put("id_menu",jsonObject.getString("id_menu").toString())
                    mn.put("nama_menu",jsonObject.getString("nama_menu"))
                    mn.put("harga",jsonObject.getString("harga"))
                    mn.put("nama_kategori",jsonObject.getString("nama_kategori"))
                    mn.put("url",jsonObject.getString("url"))
                    daftarMenu.add(mn)
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nama_menu", namaMenu)
                return  hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }
}