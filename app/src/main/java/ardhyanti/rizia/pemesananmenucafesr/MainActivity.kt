package ardhyanti.rizia.pemesananmenucafesr

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.content.Intent
import android.view.View
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), View.OnClickListener  {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnMenu.setOnClickListener(this)
        btnKategori.setOnClickListener(this)
        btnDetail.setOnClickListener(this)
        btnPesan.setOnClickListener(this)
        btnTambahMenu.setOnClickListener(this)
        btnInfo.setOnClickListener(this)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnMenu->{
                val pindah = Intent(this,DaftarMenu::class.java)
                startActivity(pindah)
            }
            R.id.btnTambahMenu->{
                val pindah = Intent(this,Menu::class.java)
                startActivity(pindah)
            }
            R.id.btnPesan ->{
                val pindah = Intent(this,Pesanan::class.java)
                startActivity(pindah)
            }
            R.id.btnDetail->{

            }
            R.id.btnKategori -> {
                val pindah = Intent(this,Kategori::class.java)
                startActivity(pindah)
            }
            R.id.btnInfo -> {
                val pindah = Intent(this,About::class.java)
                startActivity(pindah)
            }
        }
    }
}