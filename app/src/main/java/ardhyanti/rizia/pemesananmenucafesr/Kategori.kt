package ardhyanti.rizia.pemesananmenucafesr

import android.icu.text.SimpleDateFormat
import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.kategori.*
import org.json.JSONArray
import org.json.JSONObject
import kotlin.collections.HashMap

class Kategori : AppCompatActivity() , View.OnClickListener {

    lateinit var adapKategori: AdapKategori
    var id_kategori : String =""
    var daftarkategori = mutableListOf<HashMap<String,String>>()
    var uri1 = "http://192.168.43.239/UasAndroid/show_kategori.php"
    var uri2 = "http://192.168.43.239/UasAndroid/query_kategori.php"
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.kategori)
        adapKategori =  AdapKategori(daftarkategori,this)
        lsKategori.layoutManager = LinearLayoutManager(this)
        lsKategori.adapter = adapKategori

        btnInsert.setOnClickListener(this)
        btnUpdate.setOnClickListener(this)
        btnDelete.setOnClickListener(this)
    }

    override fun onStart() {
        super.onStart()
        ShowKategori()
    }

    fun ShowKategori(){
        val request = StringRequest(Request.Method.POST,uri1, Response.Listener { response ->
            daftarkategori.clear()
            adapKategori.notifyDataSetChanged()
            val jsonArray = JSONArray(response)
            for (x in 0..(jsonArray.length()-1)){
                val jsonObject = jsonArray.getJSONObject(x)
                var kategori = HashMap<String,String>()
                kategori.put("nama_kategori",jsonObject.getString("nama_kategori"))
                kategori.put("id_kategori",jsonObject.getInt("id_kategori").toString())
                daftarkategori.add(kategori)

            }
            adapKategori.notifyDataSetChanged()

        }, Response.ErrorListener { error ->
            Toast.makeText(this,"Kesalahan Saat Konfigurasi", Toast.LENGTH_SHORT).show()
        })
        val queue =  Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,uri2,
            Response.Listener { response ->

                val jsonobject  = JSONObject(response)
                val error = jsonobject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this,"Operasi Berhasil ", Toast.LENGTH_LONG).show()
                    ShowKategori()
                }else{
                    Toast.makeText(this,"Operasi Gagal ", Toast.LENGTH_LONG).show()
                } },
            Response.ErrorListener { error ->
            })
        {
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert"->{
                        hm.put("mode","insert")
                        hm.put("nama_kategori",namaKat.text.toString())
                    }

                    "delete"->{
                        hm.put("mode","delete")
                        hm.put("id_kategori",id_kategori)
                    }
                    "update"->{
                        hm.put("mode","update")
                        hm.put("nama_kategori",namaKat.text.toString())
                        hm.put("id_kategori",id_kategori)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert->{
                query("insert")
            }
            R.id.btnUpdate->{
                query("update")
            }
            R.id.btnDelete->{
                query("delete")
            }
        }
    }



}