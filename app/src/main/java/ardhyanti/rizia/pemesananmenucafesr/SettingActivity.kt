package ardhyanti.rizia.pemesananmenucafesr

import android.content.Context
import android.content.Intent
import android.content.SharedPreferences
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.SeekBar
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import kotlinx.android.synthetic.main.seting_activity.*

class SettingActivity  : AppCompatActivity(), View.OnClickListener {

    lateinit var pref : SharedPreferences
    val arrayWarnaBackground = arrayOf("Blue", "Yellow", "Green", "Black")
    lateinit var adapterSpin: ArrayAdapter<String>
    var pilihWarnaheader = ""
    var pilihWarnabagroun = ""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.seting_activity)
        seekBar.setOnSeekBarChangeListener(onSeek)
        adapterSpin = ArrayAdapter(this, android.R.layout.simple_list_item_1, arrayWarnaBackground)
        spinner.adapter = adapterSpin
        button.setOnClickListener(this)

        // get pilihan dari spinner
        spinner.onItemSelectedListener = object : AdapterView.OnItemSelectedListener{
            override fun onNothingSelected(parent: AdapterView<*>?) {
                Toast.makeText(baseContext,"Tidak ada yang dipilih",Toast.LENGTH_SHORT).show()
            }

            override fun onItemSelected(
                parent: AdapterView<*>?,
                view: View?,
                position: Int,
                id: Long
            ) {
                Toast.makeText(baseContext,adapterSpin.getItem(position),Toast.LENGTH_SHORT).show()
                pilihWarnabagroun = adapterSpin.getItem(spinner.selectedItemPosition).toString()
            }
        }
        // get isi radio group untuk warna
        radioGroup.setOnCheckedChangeListener { group, checkedId ->
            when(checkedId){
                R.id.blue -> pilihWarnaheader = "biru"
                R.id.black-> pilihWarnaheader = "hitam"
                R.id.yellow-> pilihWarnaheader = "kuning"
                R.id.green -> pilihWarnaheader = "hijau"

            }
        }

        pref = getSharedPreferences("setting", Context.MODE_PRIVATE)
        seekBar.progress = pref.getInt("font_size_header",44)
        seekBar.setOnSeekBarChangeListener(onSeek)
        detIsi.setText("${pref.getString("teks_isi","Tempat Nongkrong kekinian Free WiFi, Nyaman, Murah, Smoking Area")}")
    }

    val onSeek = object : SeekBar.OnSeekBarChangeListener{
        override fun onProgressChanged(seekBar: SeekBar?, progress: Int, fromUser: Boolean) {
            progress.toFloat()
        }
        override fun onStartTrackingTouch(seekBar: SeekBar?) {
        }
        override fun onStopTrackingTouch(seekBar: SeekBar?) {
        }
    }

    override fun onClick(v: View?) {
        pref = getSharedPreferences("setting", Context.MODE_PRIVATE)
        val edit = pref.edit()
        edit.putInt("font_size_header",seekBar.progress)
        edit.putString("teks_isi",detIsi.text.toString())
        edit.putString("warnahed",pilihWarnaheader)
        edit.putString("warnabacg",pilihWarnabagroun)
        edit.commit()
        Toast.makeText(this,pilihWarnaheader.toString(),Toast.LENGTH_SHORT).show()
        val ina  = Intent(this,MainActivity::class.java)
        startActivity(ina)
    }

}

