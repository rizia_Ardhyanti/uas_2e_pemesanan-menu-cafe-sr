package ardhyanti.rizia.pemesananmenucafesr

import android.app.Activity
import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import com.android.volley.Request
import com.android.volley.Response
import com.android.volley.toolbox.StringRequest
import com.android.volley.toolbox.Volley
import kotlinx.android.synthetic.main.pemesanan.*
import org.json.JSONArray
import org.json.JSONObject
import java.text.SimpleDateFormat
import java.util.*
import kotlin.collections.HashMap

class Pesanan : AppCompatActivity(), View.OnClickListener {
    override fun onClick(v: View?) {
        when(v?.id){
            R.id.btnInsert3 ->{
                query("insert")
            }
            R.id.btnDelete3->{
                query("delete")
            }
        }
    }
    lateinit var pesananAdapter : AdapPesanan
    lateinit var menuAdapter : ArrayAdapter<String>
    var daftarPesanan = mutableListOf<HashMap<String,String>>()
    var daftarMenu = mutableListOf<String>()
    val url = "http://192.168.43.239/UasAndroid/show_pesanan.php"
    val url2 = "http://192.168.43.239/UasAndroid/show_data.php"
    val url3 = "http://192.168.43.239/UasAndroid/query_pesanan.php"
    var pilihMenu=""
    var id_pesanan : String =""
    var var1 : String =""

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.pemesanan)
        pesananAdapter = AdapPesanan(daftarPesanan, this)
        lsPesan.layoutManager = LinearLayoutManager(this)
        lsPesan.adapter = pesananAdapter

        menuAdapter = ArrayAdapter(this,android.R.layout.simple_dropdown_item_1line,
            daftarMenu)
        spinner2.adapter = menuAdapter
        spinner2.onItemSelectedListener = itemSelected

        btnInsert3.setOnClickListener(this)
        btnDelete3.setOnClickListener(this)
        btnTotal.setOnClickListener {
            var1 = edHarga.text.toString()
            edTotal.setText(var1) }
    }

    override fun onStart() {
        super.onStart()
        showDataPesanan("")
        getMenu()
    }

    val itemSelected = object : AdapterView.OnItemSelectedListener{
        override fun onNothingSelected(parent: AdapterView<*>?) {
            spinner2.setSelection(0)
            pilihMenu = daftarMenu.get(0)
        }
        override fun onItemSelected(parent: AdapterView<*>?, view: View?, position: Int, id: Long) {
            pilihMenu = daftarMenu.get(position)
        }
    }

    fun query(mode : String){
        val request = object : StringRequest(Method.POST,url3,
            Response.Listener { response ->
                val jsonObject = JSONObject(response)
                val error = jsonObject.getString("kode")
                if (error.equals("000")){
                    Toast.makeText(this, "Operasi berhasil", Toast.LENGTH_LONG).show()
                    showDataPesanan("")
                }else{
                    Toast.makeText(this, "Operasi GAGAL", Toast.LENGTH_LONG).show()
                }
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Tidak dapat terhubung ke server", Toast.LENGTH_LONG)
                    .show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                when(mode){
                    "insert" ->{
                        hm.put("mode","insert")
                        hm.put("nomer_meja", edNoMeja.text.toString())
                        hm.put("harga", edHarga.text.toString())
                        hm.put("total", edTotal.text.toString())
                        hm.put("nama_menu", pilihMenu)
                    }
                    "update" ->{
                        hm.put("mode","update")
                        hm.put("id_pesanan", id_pesanan)
                        hm.put("nomer_meja", edNoMeja.text.toString())
                        hm.put("harga", edHarga.text.toString())
                        hm.put("total", edTotal.text.toString())
                        hm.put("nama_menu", pilihMenu)
                    }
                    "delete" ->{
                        hm.put("mode","delete")
                        hm.put("id_pesanan", id_pesanan)
                    }
                }
                return hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun getMenu(){
        val request = StringRequest(
            Request.Method.POST,url2,
            Response.Listener { response ->
                daftarMenu.clear()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    daftarMenu.add(jsonObject.getString("nama_menu"))
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->  })
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }

    fun showDataPesanan(nomerMeja : String){
        val request = object : StringRequest(
            Request.Method.POST,url,
            Response.Listener { response ->
                daftarPesanan.clear()
                val jsonArray = JSONArray(response)
                for(x in 0..(jsonArray.length()-1)){
                    val jsonObject = jsonArray.getJSONObject(x)
                    var mn = HashMap<String,String>()
                    mn.put("id_pesanan",jsonObject.getString("id_pesanan").toString())
                    mn.put("nomer_meja",jsonObject.getString("nomer_meja"))
                    mn.put("harga",jsonObject.getString("harga"))
                    mn.put("total",jsonObject.getString("total"))
                    mn.put("nama_menu",jsonObject.getString("nama_menu"))
                    daftarPesanan.add(mn)
                }
                menuAdapter.notifyDataSetChanged()
            },
            Response.ErrorListener { error ->
                Toast.makeText(this,"Terjadi kesalahan koneksi ke server", Toast.LENGTH_SHORT).show()
            }){
            override fun getParams(): MutableMap<String, String> {
                val hm = HashMap<String,String>()
                hm.put("nomer_meja", nomerMeja)
                return  hm
            }
        }
        val queue = Volley.newRequestQueue(this)
        queue.add(request)
    }


}